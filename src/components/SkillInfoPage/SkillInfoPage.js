import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";

import { Button, Form } from "react-bootstrap";
import { Form as ReduxForm, Control } from "react-redux-form";
import "./SkillInfoPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const SkillInfoPage = ({ match, onLoad, handleSubmit, deleteSkill }) => {
  useEffect(() => {
    onLoad(match.params.id);
  }, [match, onLoad]);
  return (
    <div>
      <BeginingNavBar />
      <div className="Content">
        <Link to="/admin/skill" className="btn btn-success">
          Return to Skill List
        </Link>
        <ReduxForm model="initialStateSkill" onSubmit={v => handleSubmit(v)}>
          <Form.Group controlId="formBasicUsername">
            <Form.Label>Name:</Form.Label>
            <Control.text
              model=".name"
              component={bootstrapFormControl}
              type="text"
              disabled
            />
          </Form.Group>
          <Button variant="primary" type="submit" className="ml-1">
            Update
          </Button>
          <Button
            variant="dangger"
            type="button"
            className="ml-1"
            onClick={deleteSkill(match.params.id)}
          >
            Detele
          </Button>
        </ReduxForm>
      </div>
    </div>
  );
};

export default SkillInfoPage;
