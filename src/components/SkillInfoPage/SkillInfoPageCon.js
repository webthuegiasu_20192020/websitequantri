import { connect } from "react-redux";
import {
  getProtected,
  postProtected,
  mergeFormSkill
} from "../../actions/actions";
import SkillInfoPage from "./SkillInfoPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    match: ownProps.match
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected(data, "/admin/skill/update", "/admin/skill"));
  },
  deleteSkill: data => {
    let delItem = { id: data };
    console.log(data);
    dispatch(postProtected(delItem, "/admin/skill/delete", "/admin/skill"));
  },
  onLoad: id => {
    dispatch(getProtected(`/skill/${id}`, mergeFormSkill));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SkillInfoPage);
