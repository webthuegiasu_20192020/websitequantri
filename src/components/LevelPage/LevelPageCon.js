import { connect } from "react-redux";
import LevelPage from "./LevelPage";

const mapStateToProps = state => {
  const { user } = state;
  return {
    token: user.token
  };
};

export default connect(mapStateToProps)(LevelPage);
