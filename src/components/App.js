import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import UserLogin from "./UserAccount/UserLoginCon";
import AdminPage from "./AdminPage";
import AddAdminPage from "./AddAdminPage";
import UserListPage from "./UserListPage";
import UserInfoPage from "./UserInfoPage";
import LevelPage from "./LevelPage";
import AddLevelPage from "./AddLevelPage";
import SkillPage from "./SkillPage";
import AddSkillPage from "./AddSkillPage";
import SkillInfoPage from "./SkillInfoPage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" component={UserLogin}></Route>
        <Route exact path="/admin" component={AdminPage}></Route>
        <Route exact path="/admin/add" component={AddAdminPage}></Route>
        <Route exact path="/admin/level" component={LevelPage}></Route>
        <Route exact path="/admin/skill/add" component={AddSkillPage}></Route>
        <Route exact path="/admin/skill" component={SkillPage}></Route>
        <Route exact path="/admin/level/add" component={AddLevelPage}></Route>
        <Route exact path="/admin/user" component={UserListPage}></Route>
        <Route exact path="/admin/user/:id" component={UserInfoPage}></Route>
        <Route exact path="/admin/skill/:id" component={SkillInfoPage}></Route>
      </Switch>
    </Router>
  );
}

export default App;
