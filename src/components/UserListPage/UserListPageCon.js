import { connect } from "react-redux";
import UserListPage from "./UserListPage";

const mapStateToProps = state => {
  const { apiAction, user } = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL,
    token: user.token
  };
};

export default connect(mapStateToProps)(UserListPage);
