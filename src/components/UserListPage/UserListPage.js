import React, { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Table } from "react-bootstrap";
import "./UserListPage.css";

const UserListPage = ({ isRedirect, URL, token }) => {
  let [dataAdmin, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      fetch(`/admin/user`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setData);
    };
    fetchData();
  }, [token]);
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL} />;
    }
    return null;
  };
  const dataTable = dataAdmin.map((value, index) => {
    return (
      <tr>
        <td>{index}</td>
        <td>{value.username}</td>
        <td>{value.email}</td>
        <td>
          <Link to={`/admin/user/${value.id}`} className="btn btn-primary">
            Detail
          </Link>
        </td>
      </tr>
    );
  });
  return (
    <div>
      <BeginingNavBar />
      <div>
        <div className="Content">
          <Link to="/admin" className="btn btn-success mb-3">
            Admin List
          </Link>
          <Link to="/admin/level" className="btn btn-success ml-2 mb-3">
            Level List
          </Link>
          <Link to="/admin/skill" className="btn btn-success ml-2 mb-3">
            Skill List
          </Link>
          <Link to="/admin/user" className="btn btn-success ml-2 mb-3">
            User List
          </Link>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Email</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{dataTable}</tbody>
          </Table>
        </div>
      </div>
      {link(isRedirect)}
    </div>
  );
};

export default UserListPage;
