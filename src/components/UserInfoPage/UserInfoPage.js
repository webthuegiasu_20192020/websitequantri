import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import "./UserInfoPage.css";

const UserInfoPage = ({ match, token }) => {
  console.log(match);
  console.log(token);
  const [data, setData] = useState({
    id: "",
    username: "",
    password: "",
    firstname: "",
    lastname: "",
    email: "",
    address: "",
    type: 3
  });
  useEffect(() => {
    const fetchData = async (match, token) => {
      fetch(`/admin/user/${match.params.id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setData);
    };
    fetchData(match, token);
  }, [match, token]);
  const type = type => {
    if (type === 3) {
      return "";
    } else if (type === 2) {
      return "Tutor";
    } else {
      return "Student";
    }
  };
  return (
    <div>
      <BeginingNavBar />
      <div className="Content">
        <Link to="/admin/user" className="btn btn-success">
          Return to Users List
        </Link>
        <div className="user-info mt-2">
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">UserName:</p>
            {data.username}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Email:</p>
            {data.email}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">First Name:</p>
            {data.firstname}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Last Name:</p>
            {data.lastname}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Type:</p>
            {type(data.type)}
          </div>
          <div className="d-flex flex-row">
            <p className="font-weight-bold mr-2">Address:</p>
            {data.address}
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserInfoPage;
