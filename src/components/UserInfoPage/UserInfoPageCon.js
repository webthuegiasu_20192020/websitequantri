import { connect } from "react-redux";
import UserInfoPage from "./UserInfoPage";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    token: user.token,
    match: ownProps.match
  };
};

export default connect(mapStateToProps)(UserInfoPage);
