import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Table } from "react-bootstrap";
import "./SkillPage.css";

const SkillPage = ({ token }) => {
  let [dataAdmin, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      fetch(`/skill`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setData);
    };
    fetchData();
  }, [token]);
  const dataTable = dataAdmin.map((value, index) => {
    return (
      <tr>
        <td>{index}</td>
        <td>{value.name}</td>
        <td>
          <Link to={`/admin/skill/${value.id}`} className="btn btn-primary">
            Detail
          </Link>
        </td>
      </tr>
    );
  });
  return (
    <div>
      <BeginingNavBar />
      <div>
        <div className="Content">
          <Link
            to="/admin/skill/add"
            className="btn btn-success mb-3 float-right"
          >
            Add Skill
          </Link>
          <Link to="/admin" className="btn btn-success mb-3">
            Admin List
          </Link>
          <Link to="/admin/level" className="btn btn-success ml-2 mb-3">
            Level List
          </Link>
          <Link to="/admin/skill" className="btn btn-success ml-2 mb-3">
            Skill List
          </Link>
          <Link to="/admin/user" className="btn btn-success ml-2 mb-3">
            User List
          </Link>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{dataTable}</tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default SkillPage;
