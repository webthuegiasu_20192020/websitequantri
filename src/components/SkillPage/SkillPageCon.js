import { connect } from "react-redux";
import AdminPage from "./SkillPage";

const mapStateToProps = state => {
  const { user } = state;
  return {
    token: user.token
  };
};

export default connect(mapStateToProps)(AdminPage);
