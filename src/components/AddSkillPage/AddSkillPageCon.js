import { connect } from "react-redux";
import { postProtected } from "../../actions/actions";
import AddAdminPage from "./AddSkillPage";

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/admin/skill/add", "/admin/skill", data));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(AddAdminPage);
