import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Form, Button } from "react-bootstrap";
import { Form as ReduxForm, Control, Errors } from "react-redux-form";
import "./AddSkillPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const AddAdminPage = ({ handleSubmit }) => {
  const [isRedirect, MakeRedirect] = useState(false);

  if (isRedirect) {
    return <Redirect to="/admin" />;
  }

  const handleClick = v => {
    handleSubmit(v);
    MakeRedirect(true);
  };

  return (
    <div>
      <BeginingNavBar />
      <div className="form">
        <div className="Content">
          <Link to="/admin/" className="btn btn-success">
            Return to Skill List
          </Link>
          <ReduxForm model="initialStateSkill" onSubmit={v => handleClick(v)}>
            <Form.Group controlId="formBasicAddress">
              <Form.Label>Skill Name:</Form.Label>
              <Control.text
                model=".name"
                component={bootstrapFormControl}
                type="text"
                updateOn="change"
                validators={{
                  isRequired: val => val && val.length
                }}
              />
              <Errors
                model="detailForm.address"
                className="alert alert-danger"
                show={field => field.touched && !field.focus}
                messages={{
                  isRequired: "Please input new administrator email"
                }}
              />
            </Form.Group>

            <div className="w-100" />
            <Button variant="primary" type="submit">
              Add
            </Button>
          </ReduxForm>
        </div>
      </div>
    </div>
  );
};

export default AddAdminPage;
