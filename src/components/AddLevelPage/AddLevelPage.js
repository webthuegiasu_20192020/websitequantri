import React from "react";
import { Redirect, Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Form, Button } from "react-bootstrap";
import { Form as ReduxForm, Control, Errors } from "react-redux-form";
import "./AddLevelPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const AddLevelPage = ({ handleSubmit, isRedirect, URL, getUsername }) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL} />;
    }
    return null;
  };

  return (
    <div>
      <BeginingNavBar />
      <div className="form">
        <div className="Content">
          <Link to="/admin/level" className="btn btn-success">
            Return to level List
          </Link>
          <ReduxForm model="initialLevelState" onSubmit={v => handleSubmit(v)}>
            <Form.Group controlId="formBasicAddress">
              <Form.Label>Level Name:</Form.Label>
              <Control.text
                model=".name"
                component={bootstrapFormControl}
                type="text"
                updateOn="change"
                validators={{
                  isRequired: val => val && val.length
                }}
              />
              <Errors
                model="detailForm.address"
                className="alert alert-danger"
                show={field => field.touched && !field.focus}
                messages={{
                  isRequired: "Please input new level name"
                }}
              />
            </Form.Group>
            <div className="w-100" />
            <Button variant="primary" type="submit">
              Add
            </Button>
          </ReduxForm>
        </div>
      </div>
      {link(isRedirect)}
    </div>
  );
};

export default AddLevelPage;
