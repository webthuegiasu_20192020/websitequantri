import { connect } from "react-redux";
import { postProtected } from "../../actions/actions";
import AddLevelPage from "./AddLevelPage";

const mapStateToProps = state => {
  const { apiAction } = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/admin/add", "/admin", data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddLevelPage);
