import { connect } from "react-redux";
import { actions } from "react-redux-form";
import { postProtected } from "../../actions/actions";
import AddAdminPage from "./AddAdminPage";

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/admin/add", "/admin", data));
  },
  getUsername: (model, value) => {
    const username = value.slice(0, value.indexOf("@"));
    dispatch(actions.change(model, value));
    dispatch(actions.change("tempAdminUser.username", username));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(AddAdminPage);
