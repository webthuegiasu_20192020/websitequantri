import { connect } from "react-redux";
import { logOut, redirectAPI } from "../../actions/actions";
import BeginingNavBar from "./BeginingNavBar";

const mapStateToProps = state => {
  const { apiAction } = state;
  return {
    isRedirect: apiAction.isRedirect
  };
};

const mapDispatchToProps = dispatch => ({
  handleClick: () => {
    dispatch(logOut());
    dispatch(redirectAPI("/login"));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BeginingNavBar);
