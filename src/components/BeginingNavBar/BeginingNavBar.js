import React from "react";
import {Navbar} from "react-bootstrap"
import {Link, Redirect} from "react-router-dom"
import "./BeginingNavBar.css";

const BeginningNavBar = ({ handleClick, isRedirect }) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to="/login" />;
    }
    return null;
  };
  return (
    <div className="BeginNavBar">
      <Navbar.Brand className="SignupNavBrand"><Link to="/home" style={{textDecoration: "none", color: "forestgreen"}}>Online Tutors Services</Link></Navbar.Brand>
      {link(isRedirect)}
      <button className="LogOutBtn btn btn-light" onClick={() => handleClick()}>
        Log Out
      </button>
    </div>
  );
};

export default BeginningNavBar;
