import { REDIRECT_API } from "../actions/actions";

const apiAction = (state = { isRedirect: false, URL: "" }, action) => {
  switch (action.type) {
    case REDIRECT_API:
      return {
        isRedirect: true,
        URL: action.URL
      };
    default:
      return state;
  }
};

export default apiAction;
