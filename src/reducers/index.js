import { combineReducers } from "redux";
import { createForms } from "react-redux-form";
import user from "./user";
import apiAction from "./api";
import initialStateUser from "./initialStateUser";
import tempAdminUser from "./tempAdminUser";
import initialStateSkill from './initialStateSkill';
import initialStateLevel from './initialStateLevel';

export default combineReducers({
  user,
  apiAction,
  ...createForms({
    initialStateUser: initialStateUser,
    tempAdminUser: tempAdminUser,
    initialStateLevel: initialStateLevel,
    initialStateSkill:initialStateSkill
  })
});
